#!/usr/bin/env bash

#*******************************************************************************
# Copyright (c) 2023 Eclipse Foundation and others.
# This program and the accompanying materials are made available
# under the terms of the Eclipse Public License 2.0
# which is available at http://www.eclipse.org/legal/epl-v20.html
# SPDX-License-Identifier: EPL-2.0
#*******************************************************************************

# Uses DataTables (https://datatables.net/)

# Bash strict-mode
set -o errexit
set -o nounset
set -o pipefail

#set -x
html_file="html/index.html"
> "${html_file}"

projects=()
counter=0

fetch_jipps() {
  rm -rf jiro
  #shallow clone
  git clone --depth=1 https://github.com/eclipse-cbi/jiro.git 2> /dev/null

  # fetch JIPPs from jiro/instances folder
  for project_name in $(ls -1 jiro/instances); do
    # skip infra and webdev instances
    if [[ "${project_name}" == foundation-internal.* || "${project_name}" == "adoptium.temurin-compliance" ]]; then
      continue
    fi
    projects+=("${project_name}")
    counter="$((counter + 1))"
  done
  echo "No of projects: ${counter}"
}

create_header() {
  # output html to file
  # include header
  cat header/header.html > "${html_file}"
}

create_main() {
  # Create breadcrumbs
  cat << EOF >>  "${html_file}"
    <section class="default-breadcrumbs hidden-print breadcrumbs-default-margin" id="breadcrumb">
      <div class="container">
        <h3 class="sr-only">Breadcrumbs</h3>
        <div class="row">
          <div class="col-sm-24">
            <ol class="breadcrumb">
              <li><a href="https://www.eclipse.org/">Home</a></li>
              <li><a href="https://www.eclipse.org/projects/">Projects</a></li>
              <li class="active"><a href="https://ci.eclipse.org/">CI build services</a></li>
            </ol>
          </div>
        </div>
      </div>
    </section>
    <!-- /#breadcrumb -->
EOF

  cat << EOF >>  "${html_file}"
    <main>
      <div class="novaContent container legacy-page" id="novaContent">

<h1>Eclipse CI</h1>
<p>This is a list of all Jenkins instances that are hosted at the Eclipse Foundation as part of the <a href="https://github.com/eclipse-cbi/cbi/wiki">CBI project</a>.<br/>
You can find more information about our Jenkins setup on our <a href="https://github.com/eclipse-cbi/jiro/wiki">Jenkins wiki page</a>.</p>
<p align="right" style="font-size: 8pt"><a href="https://gitlab.eclipse.org/eclipsefdn/it/releng/ci.eclipse.org-dashboard">Source code</a></p>
EOF

  #TODO: Fix
  # create JavaScript
  printf "<script>window.addEventListener(\"load\",function() {" >> "${html_file}"
  for n in $(seq 0 1 "${counter}"); do
    name="${projects[${n}]:-}"
    short_name="${name##*.}"
    if [[ -n ${name} ]]; then
      printf "checkCI(\"%s\");" "${short_name}" >> "${html_file}"
    fi
  done
  printf "},false);</script>\n" >> "${html_file}"

  # create table
  echo "<table id="myTable" class=\"table\">" >> "${html_file}"
  cat << EOF >>  "${html_file}"
<thead>
  <tr>
    <th>Project name</th>
    <th>JIPP name</th>
    <th>Status</th>
  </tr>
</thead>
<tbody>
EOF

  start="0"
  end="$((counter - 1))"

  # create rows
  for p in $(seq ${start} 1 ${end}); do
    project_name="${projects[${p}]:-}"
    short_name="${project_name##*.}"
    if [[ -n ${project_name} ]]; then
      url="https://ci.eclipse.org/${short_name}"
      echo "<tr><td>${project_name}</td><td><a href=\"${url}\">${short_name}</a></td><td><i id=\"${short_name}-status\" aria-hidden=\"true\"></i></td></tr>" >> "${html_file}"
    fi
  done

  echo "</tbody></table>" >> "${html_file}"

  cat << EOF >> "${html_file}"
    <script src="jquery-3.7.0.min.js"></script>
    <script src="datatables.min.js"></script>

    <script>
    \$(document).ready( function () {
        new DataTable('#myTable', {
            info: false,
            paging: false,
            order: [[0, 'asc']],
            scrollCollapse: false,
            scrollY: '800px'
        });
    } );
    </script>

</div>
<br/><br/><br/><br/>
</main>
EOF
}

create_footer() {
  footer_file="footer.html"
  wget -q "https://www.eclipse.org/eclipse.org-common/themes/solstice/html_template/index.php?theme=default&layout=default-footer" -O "${footer_file}"
  sed -i 's/src="\/eclipse.org-common\//src="https:\/\/www.eclipse.org\/eclipse.org-common\//' "${footer_file}"

  # include footer
  cat "${footer_file}" >> "${html_file}"
}


#MAIN
fetch_jipps
create_header
create_main
create_footer

