@Library('common-shared') _

pipeline {

  agent any

  environment {
    APP_NAME = "ci-eclipse-org"
    NAMESPACE = "foundation-ci-eclipse-org"
    IMAGE_NAME = "eclipsefdn/ci-eclipse-org"
    CONTAINER_NAME = "ci-eclipse-org"
    ENVIRONMENT = "main"

    TAG_NAME = sh(
      script: """
        GIT_COMMIT_SHORT=\$(git rev-parse --short ${env.GIT_COMMIT})
        if [ "${env.ENVIRONMENT}" = "" ]; then
          printf \${GIT_COMMIT_SHORT}-${env.BUILD_NUMBER}
        else
          printf ${env.ENVIRONMENT}-\${GIT_COMMIT_SHORT}-${env.BUILD_NUMBER}
        fi
      """,
      returnStdout: true
    )
  }

  options {
    buildDiscarder(logRotator(numToKeepStr: '10'))
    timeout(time: 30, unit: 'MINUTES')
    disableConcurrentBuilds()
  }


  stages {
    stage('Build site') {
      steps {
        sh '''
            ./build_site.sh
        '''
      }
    }
    stage('Build docker image') {
      agent {
        label 'docker-build'
      }
      steps {
        readTrusted 'Dockerfile'
        sh '''
            DOCKER_BUILDKIT=1 docker build -f Dockerfile --no-cache -t ${IMAGE_NAME}:${TAG_NAME} -t ${IMAGE_NAME}:latest . 2> docker_build.log
        '''
        archiveArtifacts artifacts: 'docker_build.log'
      }
    }

    stage('Push docker image') {
      agent {
        label 'docker-build'
      }
      when {
        environment name: 'ENVIRONMENT', value: 'main'
      }
      steps {
        withDockerRegistry([credentialsId: 'infra-docker-bot', url: 'https://index.docker.io/v1/']) {
          sh '''
            docker push ${IMAGE_NAME}:${TAG_NAME}
            docker push ${IMAGE_NAME}:latest
          '''
        }
      }
    }

    stage('Deploy to cluster') {
      agent {
        kubernetes {
          label 'kubedeploy-agent'
          yaml '''
          apiVersion: v1
          kind: Pod
          spec:
            containers:
            - name: kubectl
              image: eclipsefdn/kubectl:okd-c1
              command:
              - cat
              tty: true
              resources:
                limits:
                  cpu: 1
                  memory: 1Gi
              volumeMounts:
              - mountPath: "/home/default/.kube"
                name: "dot-kube"
                readOnly: false
            - name: jnlp
              resources:
                limits:
                  cpu: 1
                  memory: 1Gi
            volumes:
            - name: "dot-kube"
              emptyDir: {}
          '''
        }
      }

      when {
        environment name: 'ENVIRONMENT', value: 'main'
      }
      steps {
        container('kubectl') {
          sh '''
            echo "newImageRef: ${IMAGE_NAME}:${TAG_NAME}"
          '''
          updateContainerImage([
            credentialsId: "jamstack-ci-eclipse-org",
            namespace: "${NAMESPACE}",
            selector: "app=${APP_NAME}",
            containerName: "${env.CONTAINER_NAME}",
            newImageRef: "${IMAGE_NAME}:${TAG_NAME}"
          ])
        }
      }
    }
  }

  post {
    always {
      deleteDir() /* clean up workspace */
      //sendNotifications currentBuild
    }
  }
}
