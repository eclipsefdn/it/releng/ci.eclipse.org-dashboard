#FROM nginx:mainline-alpine-slim
FROM nginxinc/nginx-unprivileged:mainline-alpine-slim
COPY html/ /usr/share/nginx/html
