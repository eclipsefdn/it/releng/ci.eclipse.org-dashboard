#!/usr/bin/env bash

#*******************************************************************************
# Copyright (c) 2023 Eclipse Foundation and others.
# This program and the accompanying materials are made available
# under the terms of the Eclipse Public License 2.0
# which is available at http://www.eclipse.org/legal/epl-v20.html
# SPDX-License-Identifier: EPL-2.0
#*******************************************************************************

# Uses DataTables (https://datatables.net/)

# Bash strict-mode
set -o errexit
set -o nounset
set -o pipefail

header_file="header.html.new"

fetch_header() {
  echo "Fetching header..."
  wget -q "https://www.eclipse.org/eclipse.org-common/themes/solstice/html_template/index.php?theme=default&layout=default-header" -O "${header_file}"

  # Fix titles
  sed -i 's/href="\/eclipse.org-common\//href="https:\/\/www.eclipse.org\/eclipse.org-common\//' "${header_file}"
  sed -i 's/content="HTML Template |/content="Eclipse CI |/' "${header_file}"
  sed -i 's/<title>HTML Template |/<title>Eclipse CI |/' "${header_file}"
  sed -i 's/src="\/eclipse.org-common\//src="https:\/\/www.eclipse.org\/eclipse.org-common\//' "${header_file}"

  # Add checkCI method
  #TODO: simplify
  ci_script=$(cat << EOF
<script>
  var baseciurl="https://ci.eclipse.org";
  function checkCI(ci) {
      var status=document.getElementById(ci+"-status");
      var xhr = new XMLHttpRequest();
      xhr.timeout = 10000;
      xhr.open('HEAD', baseciurl+"/"+ci+"/api/xml?depth=0", true);
      xhr.onreadystatechange = function() {
        if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
          status.className = "fa fa-check-circle";
          status.style = "color:#007E33";
          status.title = xhr.statusText;
        } else if (xhr.readyState === XMLHttpRequest.DONE && xhr.status >= 400) {
          status.className = "fa fa-times-circle";
          status.style = "color:#CC0000";
          status.title = xhr.status + " " + xhr.statusText;
          }
      }
      xhr.ontimeout = function() {
          status.className = "fa fa-question-circle";
          status.style = "color:#FF8800";
          status.title = "Timeout"
      }
      xhr.send();
  }
</script>
EOF
)
  ci_script2="$(echo -n ${ci_script})" # get rid of newline
  sed -i 's|<\/head>|'"${ci_script2}"'<\/head>|' "${header_file}"

  # remove Google Analytics
  sed -i '/<!-- Google Tag Manager -->/,/<!-- End Google Tag Manager -->/d' "${header_file}"
  sed -i '/<!-- Google Tag Manager (noscript) -->/,/<!-- End Google Tag Manager (noscript) -->/d' "${header_file}"
}

fetch_header
